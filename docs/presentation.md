---
title: GraphQL and Caliban demo
author: Yon-lu
date: 2020-12-22

---

# Introduction to GraphQL

---

# Agenda

1. Start from a RESTful API
    
2. GraphQL
    
3. Caliban: a functional GraphQL library
    
4. Some live coding
    
5. GraphQL demo: Whodunit
    

---

# Prelude

Before going deeper into GraphQL, let's take a detour and look at REST

> REST: REpresentational State Transfer

---
# Frontend

Let's imagine I'm a Frontend developer, working on the website for Liverpool FC.

I've been given 2 REST endpoints `/players/` and `/games/` from the backend.

The current player profile page I'm working on requires these fields:
- player name
- country
- picture URL
- games played, and scorelines from games
    


**Scala**

(What we are trying to model)
    
```scala
case class Player(playerName: String, country: String, price: Int, role: Role, pictureUrl: Option[URL], games: List[Int])

case class Game(id: Int, teams: List[String], date: String, score: String)
```

---
# REST example

```
// GET /players/1 
```
**JSON**
```json
{
  "playerName" : "Jordan Hendenson",
  "country" : "England",
  "price" : 45000000,
  "role" : "MIDFIELDER",
  "pictureUrl" : "https://cdn.live4liverpool.com/wp-content/uploads/2019/06/rsz_2019-06-01t222317z_615534451_rc1b00483620_rtrmadp_3_soccer-champions-tot-liv-800x0-c-default.jpg",
  "games": [
      "1",
      "2",
      "3",
      "4"
  ]
}
```

- `"games"` provide us with the list of game IDs to query the `/games/` endpoint
    


---
# Issues with REST

1. The type, shape and how you fetch the data is tightly coupled to the resource
    - Server defines the schema of the resource and the client has to work around with this inflexible format
        
    - The frontend developer could be requesting for data in N number of ways for different pages, but this REST endpoint only perfectly satisfies 1 schema.

---
# Issues with REST

1. The type, shape and how you fetch the data is tightly coupled to the resource
    - Server defines the schema of the resource and the client has to work around with this inflexible format
        
    - The frontend developer could be requesting for data in N number of ways for different pages, but this REST endpoint only perfectly satisfies 1 schema.
    
        
2. **Over-fetching:**
    
    - Even if you do not require certain fields in a resource, they will be transmitted
        
    - If the client does not even use the data, there's a waste in computer power and network bandwidth
        
    - In our example, `price` and `role` were not needed. In reality, many more fields and objects could be included in a response.
---

# Issues with REST

1. The type, shape and how you fetch the data is tightly coupled to the resource
    - Server defines the schema of the resource and the client has to work around with this inflexible format
        
    - The frontend developer could be requesting for data in N number of ways for different pages, but this REST endpoint only perfectly satisfies 1 schema.
    
        
2. **Over-fetching:**
    
    - Even if you do not require certain fields in a resource, they will be transmitted
        
    - If the client does not even use the data, there's a waste in computer power and network bandwidth
        
    - In our example, `price` and `role` were not needed. In reality, many more fields and objects could be included in a response.
    
        
3. **Under-fetching:**
    
    - If the data you require from the endpoint is not available, then you would need to make multiple round trips (eg. to another endpoint) to fetch all the data required
        
    - In our example, we required more information about games, and would need to call the `/games/` endpoint. This would likely add additional latency in serving your webpage.  
        
        
**Let's proceed to get an introduction to GraphQL, and see a GraphQL implementation later on.**

---

# What is GraphQL?

- **GraphQL** is a query language for APIs
    
- Server exposes a typed schema
     
- Client asks for what they want
   
- **Supported types of queries:**
    
    - Queries (analogous to `HTTP GET`)
        
    - Mutations (analogous to `HTTP POST`)
        
    - Subscriptions (usually implemented with websockets)
        
    - However, the GraphQL exists on a separate layer from the transport layer, hence it is not limited to using `HTTP`

---

# The GraphQL sales pitch

**Building APIs that:**

- are agnostic to the data source or programming language used
    - common misconception that GraphQL is a database language like SQL


- have a strongly-typed system
    
- are more predictable to develop with, because the clients specify what fields they want.
    
- have the ability to introspect the self-documenting GraphQL schema (shown in demo later)
    
- reduce data payloads to clients and reduces network bandwidth
    
- only have one endpoint for your service. (versus many endpoints with REST)

---

# Let's take a quick look

Short demo: [Graphiql](http://127.0.0.1:8090/graphiql/?query=%7B%0A%20%20findAllPlayers%20%7B%0A%20%20%20%20playerName%0A%20%20%7D%0A%7D)

---

# The promised alternative


**GraphQL query**
```graphql
{
  findAllPlayers {
    playerName
    country
    pictureUrl
    games {
      id
      teams
      score
    }
  }
}
```

**JSON result:**

```json
{
  "data": {
    "findAllPlayers": [
      {
        "playerName": "Jordan Hendenson",
        "country": "England",
        "pictureUrl": "https://cdn.live4liverpool.com/wp-content/uploads/2019/06/rsz_2019-06-01t222317z_615534451_rc1b00483620_rtrmadp_3_soccer-champions-tot-liv-800x0-c-default.jpg",
        "games": [
          {
            "id": 1,
            "teams": [
              "Liverpool FC",
              "Crystal Palace FC"
            ],
            "score": "7-0"
          },
          {
            "id": 2,
            "teams": [
              "Liverpool FC",
              "Barcelona FC"
            ],
            "score": "4-0"
          }
        ]
      }
  // more players...
  ]
}
```
---

# In comparison to REST

**With this GraphQL example:**

- We get exactly the fields we requested. No under or over fetching.
    
- Only a single round trip
    
- Easy to use tools like Graphiql/Insomnia for testing/development purposes

---

# REST in peace?

*Is this the end of REST?*

Not really. While GraphQL is an alternative to REST, its not a definitive replacement.


The GraphQL docs also explain that GraphQL and REST can coexist, and you can abstract your REST APIs behind a GraphQL server,
by masking your REST endpoint into a GraphQL endpoint using root resolvers.

---

# Caliban: Functional GraphQL Library

- LeadIQ uses/used Sangria, but we are trying to move to Caliban because Sangria is no longer maintained
    
- Minimal boilerplate
    
- Purely functional
    
    - Strongly typed
        
    - Explicit errors
        
    - ZIO to manage functional effects (rather than `Future` in Sangria)
    
---

# Getting with GraphQL basics

The GraphQL docs are pretty good at giving a more detailed run-through of all the specifications that you can use.

I'll only be going through the basics to get started:
- object types
- enums
- accepting arguments
- managing effects

---

# Simple API

**Graphql**
```graphql
type Queries {
    randomPlayerPicture: String!
    findAllPlayers: [Player!]!
}
```


**Scala**
```scala
case class Queries(randomPlayerPicture: String, findAllPlayers: List[Player])
```
    
- **Exclamation mark** ! means mandatory 
    
- **Square brackets** `[]` indicates list

---

# Objects

**Graphql**
```graphql
type Player {
    name: String!
    country: String!
    price: Int!
    role: Role!
    pictureUrl: String
    games: [Game!]!
}
```

**Scala**
```scala
  type ID = Int

  case class Player(playerName: String,
                    country: String,
                    price: Int,
                    role: Role,
                    pictureUrl: Option[URL],
                    games: List[Game])
```

- Without an exclamation mark, it means **optional**, so we can use `Option[A]` to represent the field.

---

# Enumerations

**Graphql**
```graphql
enum Role {
    DEFENDER
    FORWARD
    GOALKEEPER
    MIDFIELDER
    OTHER
    WINGER
}
```

**Scala**
```scala
sealed trait Role

object Role {
    case object FORWARD extends Role
    case object MIDFIELDER extends Role
    case object WINGER extends Role
    case object DEFENDER extends Role
    case object GOALKEEPER extends Role
    case object OTHER extends Role
}
```

- Pretty straightforward "translation"

---

# Accepting arguments

**Graphql**
```graphql
type Queries {
    findPlayer(playerName: String): Player
}
```


**Scala**
```scala
case class PlayerNameArgs(playerName: String)
case class Queries(findPlayer: PlayerNameArgs => Option[Player])
```


Why did we not use `String => Option[Player]`?
- We need a PlayerNameArgs case class, because we need a way to name the argument `playerName` that we see in the GraphQL schema.

---

# Effects

What if we need to return an effect because we have to access a database?

**Graphql**
```graphql
type Queries {
    findPlayer(playerName: String): Player
}
```


**Scala**
```scala
case class PlayerNameArgs(playerName: String)
// We simply replaced `Option` from the previous slide with `Task`
case class Queries(findPlayer: PlayerNameArgs => Task[Player])

case class PlayerNotFound(name: String) extends Throwable
```


**Quick Recap:**
- `Task[A]` is a type alias for `ZIO[Any, Throwable, A]`
- Represents an effect that has no requirements
- May fail with a `Throwable` value, or succeed with an `A`.

---

# GraphQL Project Walkthough/Live coding

Now we're ready

---

# End of Live coding

That was a very simple way to define a implement a GraphQL API.

Caliban supports using the `http4s` to spin up a graphql server, where you can route your endpoint to the GraphQL API interpreter

---

# Demo time

It's time for **YOU** to play

---

# Demo time

```

                                                                                                                     
 _____ _                  _        ___      _                   _                                                     
|_   _| |_ ___ ___ ___   |_|___   |_  |    |_|_____ ___ ___ ___| |_ ___ ___    ___ _____ ___ ___ ___    _ _ ___       
  | | |   | -_|  _| -_|  | |_ -|   _| |_   | |     | . | . |_ -|  _| . |  _|  | .'|     | . |   | . |  | | |_ -|_ _ _ 
  |_| |_|_|___|_| |___|  |_|___|  |_____|  |_|_|_|_|  _|___|___|_| |___|_|    |__,|_|_|_|___|_|_|_  |  |___|___|_|_|_|
                                                   |_|                                          |___|                 

```


---

# Let's get our hands dirty
    
1. SSH tunnel into gpu 
```bash
ssh -L 8088:localhost:8088 gpu
```
2. Boot up Insomnia or your favourite API client. Alternatively, you can also enter `http://localhost:8088/graphiql` in your browser.
     
3. For Insomnia, select the `GraphQL Demo` workspace, or just create a new workspace
     
4. Enter the endpoint `http://localhost:8088/api/graphql/` for your request


---

# Among Us - Whodunit
    
1. What is the name of the impostor?
    
2. What are the names of the people killed by the impostor?
    
3. What is the role of the player without a picture URL?
    
4. *(BONUS)* In what location, did the player with most deaths die?
    
[Link to Graphiql](http://localhost:8088/graphiql)
For Insomnia: `http://localhost:8088/api/graphql/`

---

# Answers
    
1. What is the name of the impostor?
    
DBA Zij
    
2. What are the names of the people killed by the impostor?
    
Fredrick and Johnathan
    
3. What is the role of the player without a picture URL?
    
Baby Crewmate
    
4. *(BONUS)* In what location, did the player with most deaths die?
    
Admin

---

# Summary
    
- We saw how a REST API has its shortcomings
    
- We saw how GraphQL was fundamentally different from REST in building APIs
    
- We learnt about the type schema of GraphQL and saw how easy it was to "translate" GraphQL types to Scala types
    
- We had a live coding session to spin up a GraphQL API
    
    - Only thing *really new* was how to define the schema, and call Caliban's `RootResolver`
         
- We played detective with an Among Us GraphQL endpoint and solved some murder mysteries
    
    
---

# Credits
    
- This tutorial was heavily inspired by ghostdogpr, creator of Caliban.
    
- For next steps on how to proceed from where this presentation left off, you can look at [Part 2](https://medium.com/@ghostdogpr/graphql-in-scala-with-caliban-part-2-c7762110c0f9) of his tutorial, 
where he introduces how to use `ZQuery` to optimise database calls with GraphQL.

---

# Appendix

---

# Caliban Code Generation

Developers working with GraphQL on the client side might write queries with external tools such as Insomnia, then copy pasting the code into their projects.

Although these tools are good for testing out the API, it can be error prone especially if the schema changes.

Caliban-client has a useful tool to generate boilerplate code from an existing GraphQL schema.

---

# Steps to generate boilerplate Scala code

1. Obtain the GraphQL SDL (Schema Definition Language)
2. Run a sbt command to generate boilerplate code 
(eg. `calibanGenClient <schemaPath> <outputPath> <?scalafmtPath>`)
3. Write GraphQL queries in plain Scala by calling the generated code and combining fields.


---


# Resolver

A record of all the functions that explain how to gather every field for each function. All GraphQL queries at the endpoint first goes through the `Root` (root resolver) object.

**Scala**
```scala
// API Definition
case class FindPlayerArgs(name: String)
case class AddPlayerArgs(player: Player)
case class EditPlayerPriceArgs(name: String, price: Int)
case class Queries(findPlayer: FindPlayerArgs => IO[PlayerNotFound, Player], findAllPlayers: UIO[List[Player]], randomPlayerPicture: UIO[String])

// resolvers
val queries = Queries(
      args => findPlayer(args.name),
      findAllPlayers,
      randomPlayerPicture
)
```

So far, you've seen us use Scala case classes to define our schema.

How do we get from the Scala case classes to the GraphQL type schema and resolve it?

---

# Magnolia

Caliban uses a library called **Magnolia** to automatically create at compile time instances of a type class for any case class. 

```
// API Definition
case class FindPlayerArgs(name: String)
case class AddPlayerArgs(player: Player)
case class EditPlayerPriceArgs(name: String, price: Int)
case class Queries(findPlayer: FindPlayerArgs => IO[PlayerNotFound, Player], findAllPlayers: UIO[List[Player]], randomPlayerPicture: UIO[String])

// resolvers
val queries = Queries(
      args => findPlayer(args.name),
      findAllPlayers,
      randomPlayerPicture
)

// Introducing Caliban type `RootResolver`
val api = graphQL(RootResolver(queries))
```

---

# RootResolver

```scala
// RootResolver.scala from Caliban

/**
 * A `root resolver` contains resolvers for the 3 types of operations allowed in GraphQL: queries, mutations and subscriptions.
 *
 * A `resolver` is a simple value of the case class describing the API.
 */
case class RootResolver[+Query, +Mutation, +Subscription](
  queryResolver: Option[Query],
  mutationResolver: Option[Mutation],
  subscriptionResolver: Option[Subscription]
)
```

---

# Resources

- [GraphQL docs](https://graphql.org/learn/)
    
- [Caliban tutorial Part 1](https://medium.com/@ghostdogpr/graphql-in-scala-with-caliban-part-1-8ceb6099c3c2)
    
- [Caliban tutorial Part 2](https://medium.com/@ghostdogpr/graphql-in-scala-with-caliban-part-2-c7762110c0f9)
    
- [My Gitlab Repo](https://gitlab.com/yon-lu/graphql_demo)
    
- [Pierre Ricadat/@ghostdogpr's talk on Caliban](https://www.youtube.com/watch?v=OC8PbviYUlQ)
    