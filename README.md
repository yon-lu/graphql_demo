## GraphQL Demo

This repo consists of 3 packages:

- part1
- part2
- secret

---

### Part 1: Liverpool FC Team Manager

Simple way to get started with a GraphQL endpoint.

Data is based on football players.

---

### Part 2: Analysis of different APIs

We start with a new API, the point of this package is to show the number of DB calls when implementing our API in 4 different ways.
We use ZIO and ZQuery to optimise our queries. 

#### Impact on DB
(When we call `getOrders` for N = 20 orders, each order having 2 products)

1. Naive approach: (5*N) + 1 = 101 hits

1. Nested effects: (3*N) + 1 = 61 hits

1. ZQuery: 9 hits

1. ZQuery with batching: 3 hits

---

### Secret

This package is the Among Us GraphQL endpoint that was demonstrated during the talk.