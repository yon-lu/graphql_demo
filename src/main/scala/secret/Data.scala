package secret

import java.net.URL

import secret.Data.Role._
import secret.Data.ActionType._
import caliban.CalibanError
import caliban.CalibanError.ExecutionError

object Data {

  sealed trait Role

  object Role {
    case object CREWMATE extends Role
    case object DETECTIVE extends Role
    case object CAPTAIN extends Role
    case object IMPOSTOR extends Role
    case object BABY_CREWMATE extends Role
  }

  sealed trait ActionType

  object ActionType {
    case object FIX_WIRES extends ActionType
    case object SWIPE_CARD extends ActionType
    case object KILL_PLAYER extends ActionType
    case object SABOTAGE_LIGHTS extends ActionType
    case object REFUEL_ENGINES extends ActionType
    case object SLACKING_OFF extends ActionType
  }

  type ID = Int

  case class Player(id: ID, name: String, role: Role, pictureUrl: Option[URL], deaths: Int)

  case class Action(playerId: ID, location: String, actionType: ActionType, round: Int, playerKilledId: Option[ID])

  val playerList = List(
    Player(1, "Yon-Sherlock-lu", DETECTIVE, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-U017C257MHR-b9efbdc673cf-512")), 5),
    Player(2, "Cpt. Rohan", CAPTAIN, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-UPMUJD4HK-b6bdc38cd619-512")), 6),
    Player(3, "Clement", CREWMATE, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-UFMFWHM6J-4d87667a447c-512")), 7),
    Player(4, "Jonathan", BABY_CREWMATE, None, 8),
    Player(5, "DBA Zij", IMPOSTOR, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-U012FCJ40CB-c7d623608b00-512")), 9),
    Player(6, "James", BABY_CREWMATE, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-USGNH380G-331c52692ee0-512")), 10),
    Player(7, "Fredrick", CREWMATE, Some(new URL("https://ca.slack-edge.com/T07G4GEKD-UH8CFEC01-5e82e2a81b19-512")), 11)
  )

  val actionList = List(
    Action(1, "Cafe", FIX_WIRES, 1, None),
    Action(2, "Admin", SWIPE_CARD, 2, None),
    Action(3, "Upper Engine", REFUEL_ENGINES, 2, None),
    Action(4, "Electrical", SLACKING_OFF, 1, None),
    Action(5, "Electrical", KILL_PLAYER, 1, Some(4)),
    Action(7, "Security", FIX_WIRES, 1, None),
    Action(1, "Cafe", FIX_WIRES, 2, None),
    Action(5, "Electrical", SABOTAGE_LIGHTS, 1, None),
    Action(6, "Medical Bay", SLACKING_OFF, 2, None),
    Action(7, "Cafe", FIX_WIRES, 2, None),
    Action(5, "Admin", KILL_PLAYER, 3, Some(7))
  )
}
