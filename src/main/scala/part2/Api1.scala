package part2

import part2.models.Data._
import zio.UIO

object Api1 {
  case class QueryArgs(count: Int)
  case class Query(orders: QueryArgs => UIO[List[OrderView]])

  case class OrderView(id: OrderId, customer: Customer, products: List[ProductOrderView])
  case class ProductOrderView(id: ProductId, details: ProductDetailsView, quantity: Int)
  case class ProductDetailsView(name: String, description: String, reviews: List[Review])


  def resolver(dbService: DBService): Query = {

    def getOrders(count: Int): UIO[List[OrderView]] =
      dbService
          .getLastOrders(count) // 1 api call
          .flatMap(
            UIO.foreach(_)(
              order =>
                for {
                  customer <- dbService.getCustomer(order.customerId) // N api calls
                  products <- getProducts(order.products) // each order has 2 products, 2 * N api calls
                } yield OrderView(order.id, customer, products)
            )
          )

    def getProducts(products: List[(ProductId, Int)]): UIO[List[ProductOrderView]] =
      UIO.foreach(products) {
        case (productId, quantity) =>
        dbService
          .getProduct(productId)
          .flatMap(
            product =>
              dbService
                .getReviews(product.reviews)  // each order has 2 products, each product takes 1 api call to get reviews, 2 * N api calls
              .map(reviews => ProductDetailsView(product.name, product.description, reviews)
            )
              .map(details => ProductOrderView(productId, details, quantity))
        )
      }

    Query(args => getOrders(args.count))
  }


}
