package part2.models

object Data {
  type ProductId = Int
  type OrderId = Int
  type CustomerId = Int
  type ReviewId = Int

  case class Order(id: OrderId, customerId: CustomerId, products: List[(ProductId, Int)])

  case class Customer(id: CustomerId, firstName: String, lastName: String)

  case class Product(id: ProductId, name: String, description: String, reviews: List[ReviewId])

  case class Review(id: ReviewId, rating: Int, description: String)

}
