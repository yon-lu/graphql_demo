package part2

import part2.models.Data._
import zio.{Ref, UIO}

import scala.util.Random

class DBService(dbHits: Ref[Int]) {

  def getReview(reviewId: ReviewId): UIO[Review] =
    dbHits.update(_ + 1).as(Review(reviewId, Random.between(1, 6), "some review"))

  def getReviews(reviewIds: List[ReviewId]): UIO[List[Review]] =
    dbHits.update(_ + 1).as(reviewIds.map(id => Review(id, Random.between(1, 6), "some review")))

  def getProduct(productId: ProductId): UIO[Product] =
    dbHits.update(_ + 1).as(Product(productId, "some product", "some description", List(productId % 2, (productId + 1) % 3)))

  def getProducts(productIds: List[ProductId]): UIO[List[Product]] =
    dbHits.update(_ + 1).as(productIds.map(id => Product(id, "some product", "some description", List(id % 2, (id + 1) % 3))))

  def getCustomer(id: CustomerId): UIO[Customer] =
    dbHits.update(_ + 1).as(Customer(id, "first name", "last name"))

  def getCustomers(ids: List[CustomerId]): UIO[List[Customer]] =
    dbHits.update(_ + 1).as(ids.map(id => Customer(id, "first name", "last name")))

  def getLastOrders(count: Int): UIO[List[Order]] =
    dbHits.update(_ + 1).as((1 to count).toList.map(id => Order(id, id % 3, List((id % 5, 1), (id % 2, 1)))))

  val hits: UIO[Int] = dbHits.get

}


object DBService {
  def apply(): UIO[DBService] =
    for {
    dbHits <- Ref.make(0)
    } yield new DBService(dbHits)
}