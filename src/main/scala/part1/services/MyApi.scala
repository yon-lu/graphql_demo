package part1.services

import scala.util.Try
import java.net.URL

import caliban.CalibanError.ExecutionError
import caliban.GraphQL.graphQL
import caliban.RootResolver
import caliban.schema.{ArgBuilder, Schema}
import part1.models.Data.{Player, PlayerNotFound}
import zio.{IO, UIO}
import part1.services.PlayerService.dummy._

object MyApi {

  // support for URL
  implicit val urlSchema: Schema[Any, URL] = Schema.stringSchema.contramap(_.toString)
  implicit val urlArgBuilder: ArgBuilder[URL] = ArgBuilder.string.flatMap(
    url => Try(new URL(url)).fold(_ => Left(ExecutionError(s"Invalid URL $url")), Right(_))
  )

  // API Definition
  case class FindPlayerArgs(name: String)
  case class AddPlayerArgs(player: Player)
  case class EditPlayerPriceArgs(name: String, price: Int)
  case class Queries(findPlayer: FindPlayerArgs => IO[PlayerNotFound, Player], findAllPlayers: UIO[List[Player]], randomPlayerPicture: UIO[String])
  case class Mutations(addPlayer: AddPlayerArgs => UIO[Unit], editPlayerPrice: EditPlayerPriceArgs => IO[PlayerNotFound, Unit])

  // resolvers
  val queries = Queries(
      args => findPlayer(args.name),
      findAllPlayers,
      randomPlayerPicture
    )

  val mutations = Mutations(
      args => addPlayer(args.player),
      args => editPlayerPrice(args.name, args.price)
    )

  // Introducing Caliban type `RootResolver`
  val api = graphQL(RootResolver(queries, mutations))

  val interpreter = api.interpreter
}