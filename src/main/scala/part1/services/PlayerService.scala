package part1.services

import java.net.URL

import part1.models.Data.Role.MIDFIELDER
import part1.models.Data._
import zio.{IO, UIO}

trait PlayerService {
  def findPlayer(name: String): IO[PlayerNotFound, Player]
  def findAllPlayers: UIO[List[Player]]
  def randomPlayerPicture: UIO[String]
  def addPlayer(player: Player): UIO[Unit]
  def editPlayerPrice(name: String, price: Int): IO[PlayerNotFound, Unit]
}

object PlayerService {
  val dummy: PlayerService = new PlayerService {
    override def findPlayer(name: String): IO[PlayerNotFound, Player] =
      IO.succeed(
        Player(
          "Jordan Hendenson",
          "England",
          45000000,
          MIDFIELDER,
          Some(new URL("https://cdn.live4liverpool.com/wp-content/uploads/2019/06/rsz_2019-06-01t222317z_615534451_rc1b00483620_rtrmadp_3_soccer-champions-tot-liv-800x0-c-default.jpg")),
          List(
            Game(
              "1",
              List("Liverpool FC", "Crystal Palace FC"),
              "2020-12-19",
              "7-0"
            ),
            Game(
              "2",
              List("Liverpool FC", "Barcelona FC"),
              "2019-08-5",
              "4-0"
            )
          )
        )
      )

    override def findAllPlayers: UIO[List[Player]] = IO.succeed(
      List(
        Player(
          "Jordan Hendenson",
          "England",
          45000000,
          Role.MIDFIELDER,
          Some(new URL("https://cdn.live4liverpool.com/wp-content/uploads/2019/06/rsz_2019-06-01t222317z_615534451_rc1b00483620_rtrmadp_3_soccer-champions-tot-liv-800x0-c-default.jpg")),
          List(
            Game(
              "1",
              List("Liverpool FC", "Crystal Palace FC"),
              "2020-12-19",
              "7-0"
            ),
            Game(
              "2",
              List("Liverpool FC", "Barcelona FC"),
              "2019-08-5",
              "4-0"
            )
          )

        ),
        Player(
          "Virgil van Dijk",
          "Netherlands",
          90000000,
          Role.DEFENDER,
          Some(new URL("https://images2.minutemediacdn.com/image/upload/c_fill,w_912,h_516,f_auto,q_auto,g_auto/shape/cover/sport/uefa-champions-league-tottenham-hotspur-fc-v-liverpool-fc-5dcbdccff49a6656e700002a.jpg")),
          List(
            Game(
              "2",
              List("Liverpool FC", "Barcelona FC"),
              "2019-08-5",
              "4-0"
            )
          )
        )
      )
    )

    override def randomPlayerPicture: UIO[String] =
      UIO.succeed("https://images2.minutemediacdn.com/image/upload/c_fill,w_912,h_516,f_auto,q_auto,g_auto/shape/cover/sport/uefa-champions-league-tottenham-hotspur-fc-v-liverpool-fc-5dcbdccff49a6656e700002a.jpg")

    override def addPlayer(player: Player): UIO[Unit] = UIO.unit

    override def editPlayerPrice(name: String, price: Int): IO[PlayerNotFound, Unit] = IO.fail(PlayerNotFound(name))
  }

}
