package part1.models

import java.net.URL

object Data {

  sealed trait Role

  object Role {
    case object FORWARD extends Role
    case object MIDFIELDER extends Role
    case object WINGER extends Role
    case object DEFENDER extends Role
    case object GOALKEEPER extends Role
    case object OTHER extends Role
  }

  case class Player(playerName: String, country: String, price: Int, role: Role, pictureUrl: Option[URL], games: List[Game])

  case class Game(id: String, teams: List[String], date: String, score: String)

  case class PlayerNotFound(name: String) extends Throwable

}
