package livecoding

import java.net.URL

import zio._
import caliban.CalibanError.ExecutionError
import caliban.GraphQL.graphQL
import caliban.RootResolver
import caliban.schema.{ArgBuilder, Schema}
import livecoding.MyApi.{Player, PlayerView}

import scala.util.Try

trait Service {
  def findAllPlayers: UIO[List[PlayerView]]
  def findPlayer(name: String): Task[Player]

}

object MyApi {

  // Data Models
  sealed trait Role

  object Role {
    case object FORWARD extends Role
    case object MIDFIELDER extends Role
    case object WINGER extends Role
    case object DEFENDER extends Role
    case object GOALKEEPER extends Role
    case object OTHER extends Role
  }

  type ID = Int

  case class Player(playerName: String, country: String, price: Int, role: Role, pictureUrl: Option[URL], games: List[Int])

  case class Game(id: Int, teams: List[String], date: String, score: String)

  case class PlayerView(playerName: String, country: String, price: Int, role: Role, pictureUrl: Option[URL], games: List[Game])

  case class PlayerNotFound(name: String) extends Throwable

  // Dummy data
  val playerList = List(
      Player(
        "Jordan Hendenson",
        "England",
        45000000,
        Role.MIDFIELDER,
        Some(new URL("https://cdn.live4liverpool.com/wp-content/uploads/2019/06/rsz_2019-06-01t222317z_615534451_rc1b00483620_rtrmadp_3_soccer-champions-tot-liv-800x0-c-default.jpg")),
        List(
          1,
          2
        )

      ),
      Player(
        "Virgil van Dijk",
        "Netherlands",
        90000000,
        Role.DEFENDER,
        Some(new URL("https://images2.minutemediacdn.com/image/upload/c_fill,w_912,h_516,f_auto,q_auto,g_auto/shape/cover/sport/uefa-champions-league-tottenham-hotspur-fc-v-liverpool-fc-5dcbdccff49a6656e700002a.jpg")),
        List(
          2
        )
      )
  )

  val gameList = List(
    Game(
      1,
      List("Liverpool FC", "Crystal Palace FC"),
      "2020-12-19",
      "7-0"
    ),
    Game(
      1,
      List("Liverpool FC", "Barcelona FC"),
      "2019-08-5",
      "4-0"
    )
  )

  // Services

  val service: Service = new Service {
    override def findAllPlayers: UIO[List[PlayerView]] = IO.succeed(
      playerList.map(player =>
      PlayerView(
        player.playerName,
        player.country,
        player.price,
        player.role,
        player.pictureUrl,
        player.games.map(id => gameList.find(_.id == id).get)
      ))
    )

    override def findPlayer(name: String): Task[Player] =
      playerList.find(_.playerName == name) match {
        case Some(player) => IO.succeed(player)
        case None => IO.fail(PlayerNotFound(name))
      }
  }




  // GraphQL API

  // support for URL
  implicit val urlSchema: Schema[Any, URL] = Schema.stringSchema.contramap(_.toString)
//  implicit val urlArgBuilder: ArgBuilder[URL] = ArgBuilder.string.flatMap(
//    url => Try(new URL(url)).fold(_ => Left(ExecutionError(s"Invalid URL $url")), Right(_))
//  )

  case class FindPlayerArg(name: String)
  case class Queries(findAllPlayers: UIO[List[PlayerView]], findPlayer: FindPlayerArg => Task[Player])

  val queryResolver = Queries(
    service.findAllPlayers,
    args => service.findPlayer(args.name)
  )

  val api = graphQL(RootResolver(queryResolver))

  val interpreter = api.interpreter

  println(api.render)

}
